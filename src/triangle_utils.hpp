/*
        Copyright Timothy H. Keitt <tkeitt@gmail.com>, 2012

        This code is released under the same X11/MIT license as GDAL/OGR.
        See http://trac.osgeo.org/gdal/wiki/FAQGeneral#WhatlicensedoesGDALOGRuse

        The Triangle Library by Jonathan Shewchuk can be used freely, but requires
	a license if you want to sell the code to someone else.
        See http://www.cs.cmu.edu/~quake/triangle.html 
*/
#ifndef __TRIANGLE_UTILS__
#define __TRIANGLE_UTILS__

#include <gdal/ogrsf_frmts.h>

// triangle.h does not have ifndef guards, so it is included once here
extern "C"
{
	#include <triangle.h>
}

// for some reason the triangulateio struct has no defaults...
void
initTriIO(triangulateio* tio)
{
	tio->pointlist = NULL;                                               /* In / out */
	tio->pointattributelist = NULL;                                      /* In / out */
	tio->pointmarkerlist = NULL;                                          /* In / out */
	tio->numberofpoints = 0;                                            /* In / out */
	tio->numberofpointattributes = 0;                                   /* In / out */

	tio->trianglelist = NULL;                                             /* In / out */
	tio->triangleattributelist = NULL;                                   /* In / out */
	tio->trianglearealist = NULL;                                         /* In only */
	tio->neighborlist = NULL;                                             /* Out only */
	tio->numberoftriangles = 0;                                         /* In / out */
	tio->numberofcorners = 0;                                           /* In / out */
	tio->numberoftriangleattributes = 0;                                /* In / out */

	tio->segmentlist = NULL;                                              /* In / out */
	tio->segmentmarkerlist = NULL;                                        /* In / out */
	tio->numberofsegments = 0;                                          /* In / out */

	tio->holelist = NULL;                        /* In / pointer to array copied out */
	tio->numberofholes = 0;                                      /* In / copied out */

	tio->regionlist = NULL;                      /* In / pointer to array copied out */
	tio->numberofregions = 0;                                    /* In / copied out */

	tio->edgelist = NULL;                                                 /* Out only */
	tio->edgemarkerlist = NULL;            /* Not used with Voronoi diagram; out only */
	tio->normlist = NULL;                /* Used only with Voronoi diagram; out only */
	tio->numberofedges = 0;                                             /* Out only */	
}

void
freeTriIO(triangulateio* tio)
{
	if ( tio->numberofpoints > 0 && tio->pointlist != NULL ) trifree(tio->pointlist);
	if ( tio->numberofpoints > 0 && tio->pointattributelist != NULL ) trifree(tio->pointattributelist);
	if ( tio->numberofpoints > 0 && tio->pointmarkerlist != NULL ) trifree(tio->pointmarkerlist);

	if ( tio->numberoftriangles > 0 && tio->trianglelist != NULL ) trifree(tio->trianglelist);
	if ( tio->numberoftriangles > 0 && tio->triangleattributelist != NULL ) trifree(tio->triangleattributelist);
	if ( tio->numberoftriangles > 0 && tio->trianglearealist != NULL ) trifree(tio->trianglearealist);
	if ( tio->numberoftriangles > 0 && tio->neighborlist != NULL ) trifree(tio->neighborlist);

	if ( tio->numberofsegments > 0 && tio->segmentlist != NULL ) trifree(tio->segmentlist);
	if ( tio->numberofsegments > 0 && tio->segmentmarkerlist != NULL ) trifree(tio->segmentmarkerlist);

	if ( tio->numberofholes > 0 && tio->holelist != NULL ) trifree(tio->holelist);

	if ( tio->numberofregions > 0 && tio->regionlist != NULL ) trifree(tio->regionlist);

	if ( tio->numberofedges > 0 && tio->edgelist != NULL ) trifree(tio->edgelist);
	if ( tio->numberofedges > 0 && tio->edgemarkerlist != NULL ) trifree(tio->edgemarkerlist);
	if ( tio->numberofedges > 0 && tio->normlist != NULL ) trifree(tio->normlist);

	initTriIO(tio);
}

int
write_edges_to_layer(triangulateio* tio, OGRLayer* lyr, int index_base)
{
	OGRFieldDefn ffirst("first", OFTInteger), fsecond("second", OFTInteger), fmarker("boundary", OFTInteger);

 	if( lyr->CreateField(&ffirst) != OGRERR_NONE ||
            lyr->CreateField(&fsecond) != OGRERR_NONE ||
            lyr->CreateField(&fmarker) != OGRERR_NONE )
 	{
        	std::cerr << "Unable to create output field" << std::endl;
        	return 1;
    	}

	OGRFeature* edge_ftr = OGRFeature::CreateFeature(lyr->GetLayerDefn());

	for ( int i = 0; i < tio->numberofedges; ++i )
	{

		OGRLineString line;

		int p1 = tio->edgelist[2 * i];
		int p2 = tio->edgelist[2 * i + 1];

		assert(p1 < tio->numberofpoints);
		assert(p2 < tio->numberofpoints);

		edge_ftr->SetField("first", p1 + index_base);
		edge_ftr->SetField("second", p2 + index_base);
		edge_ftr->SetField("boundary", tio->edgemarkerlist[i]); 

		line.addPoint(tio->pointlist[2 * p1], tio->pointlist[2 * p1 + 1]);
		line.addPoint(tio->pointlist[2 * p2], tio->pointlist[2 * p2 + 1]);

		edge_ftr->SetGeometry(&line);

		if( lyr->CreateFeature(edge_ftr) != OGRERR_NONE )
        	{
			std::cerr << "Failed to write feature to output" << std::endl;
			OGRFeature::DestroyFeature(edge_ftr);
           		return 1;
        	}

	}

	OGRFeature::DestroyFeature(edge_ftr);
	
	return 0;
}

int
write_points_to_layer(triangulateio* tio, OGRLayer* lyr)
{
	OGRFieldDefn fmarker("boundary", OFTInteger);

 	if( lyr->CreateField(&fmarker) != OGRERR_NONE )
 	{
        	std::cerr << "Unable to create output field" << std::endl;
        	return 1;
    	}

	OGRFeature* point_ftr = OGRFeature::CreateFeature(lyr->GetLayerDefn());

	for ( int i = 0; i < tio->numberofpoints; ++i )
	{

		OGRPoint pt;

		pt.setX(tio->pointlist[2 * i]);
		pt.setY(tio->pointlist[2 * i + 1]);

		point_ftr->SetField("boundary", tio->pointmarkerlist[i]); 

		point_ftr->SetGeometry(&pt);

		if( lyr->CreateFeature(point_ftr) != OGRERR_NONE )
        	{
			std::cerr << "Failed to write feature to output" << std::endl;
			OGRFeature::DestroyFeature(point_ftr);
           		return 1;
        	}

	}

	OGRFeature::DestroyFeature(point_ftr);
	
	return 0;
}

int
write_voronoi_edges_to_layer(triangulateio* tio, OGRLayer* lyr, int index_base)
{
	OGRFieldDefn ffirst("first", OFTInteger), fsecond("second", OFTInteger),
                     fnormx("norm_x", OFTReal), fnormy("norm_y", OFTReal);

 	if( lyr->CreateField(&ffirst) != OGRERR_NONE ||
            lyr->CreateField(&fsecond) != OGRERR_NONE ||
	    lyr->CreateField(&fnormx) != OGRERR_NONE ||
	    lyr->CreateField(&fnormy) != OGRERR_NONE )
 	{
        	std::cerr << "Unable to create output field" << std::endl;
        	return 1;
    	}

	OGRFeature* edge_ftr = OGRFeature::CreateFeature(lyr->GetLayerDefn());

	for ( int i = 0; i < tio->numberofedges; ++i )
	{

		OGRLineString line;

		int p1 = tio->edgelist[2 * i];
		int p2 = tio->edgelist[2 * i + 1];

		assert(p1 < tio->numberofpoints);
		assert(p2 < tio->numberofpoints);

		edge_ftr->SetField("first", p1 + index_base);
		edge_ftr->SetField("second", p2 + index_base);
		edge_ftr->SetField("norm_x", tio->normlist[2 * i]);
		edge_ftr->SetField("norm_y", tio->normlist[2 * i + 1]);

		line.addPoint(tio->pointlist[2 * p1], tio->pointlist[2 * p1 + 1]);
		line.addPoint(tio->pointlist[2 * p2], tio->pointlist[2 * p2 + 1]);

		edge_ftr->SetGeometry(&line);

		if( lyr->CreateFeature(edge_ftr) != OGRERR_NONE )
        	{
			std::cerr << "Failed to write feature to output" << std::endl;
			OGRFeature::DestroyFeature(edge_ftr);
           		return 1;
        	}

	}

	OGRFeature::DestroyFeature(edge_ftr);
	
	return 0;
}

int
write_voronoi_points_to_layer(triangulateio* tio, OGRLayer* lyr)
{
	OGRFeature* point_ftr = OGRFeature::CreateFeature(lyr->GetLayerDefn());

	for ( int i = 0; i < tio->numberofpoints; ++i )
	{

		OGRPoint pt;

		pt.setX(tio->pointlist[2 * i]);
		pt.setY(tio->pointlist[2 * i + 1]);

		point_ftr->SetGeometry(&pt);

		if( lyr->CreateFeature(point_ftr) != OGRERR_NONE )
        	{
			std::cerr << "Failed to write feature to output" << std::endl;
			OGRFeature::DestroyFeature(point_ftr);
           		return 1;
        	}

	}

	OGRFeature::DestroyFeature(point_ftr);
	
	return 0;
}

int
write_triangles_to_layer(triangulateio* tio, OGRLayer* lyr, int index_base)
{
	
	OGRFieldDefn ffirst("first", OFTInteger), fsecond("second", OFTInteger), fthird("third", OFTInteger);

	// Fix me should include vertex pointers
 	if( lyr->CreateField(&ffirst) != OGRERR_NONE ||
            lyr->CreateField(&fsecond) != OGRERR_NONE ||
            lyr->CreateField(&fthird) != OGRERR_NONE )
 	{
        	std::cerr << "Unable to create output field" << std::endl;
        	return 1;
    	}

	OGRFeature* tri_ftr = OGRFeature::CreateFeature(lyr->GetLayerDefn());

	int n = tio->numberofcorners;

	assert(n >= 3);

	if ( n > 3 ) std::cerr << "Warning -- non-linear elements present in triangle list" << std::endl;

	for ( int i = 0; i < tio->numberoftriangles; ++i )
	{

		OGRLinearRing tri;

		for ( int j = 0; j < n; ++j )
		{
			assert(tio->trianglelist[n * i + j] < tio->numberofpoints);
			tri.addPoint(tio->pointlist[2 * tio->trianglelist[n * i + j]],
                           	     tio->pointlist[2 * tio->trianglelist[n * i + j] + 1]);
		}

		tri_ftr->SetField("first", tio->neighborlist[3 * i] + index_base);
		tri_ftr->SetField("second", tio->neighborlist[3 * i + 1] + index_base);
		tri_ftr->SetField("third", tio->neighborlist[3 * i + 2] + index_base); 

		OGRPolygon poly;
		poly.addRing(&tri);
		poly.closeRings();

		tri_ftr->SetGeometry(&poly);

		if( lyr->CreateFeature(tri_ftr) != OGRERR_NONE )
        	{
			std::cerr << "Failed to write feature to output" << std::endl;
			OGRFeature::DestroyFeature(tri_ftr);
           		return 1;
        	}

	}

	OGRFeature::DestroyFeature(tri_ftr);
	
	return 0;
}

#endif // __TRIANGLE_UTILS__

