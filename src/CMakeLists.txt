# Boost libraries that need linking.
find_package (Boost REQUIRED COMPONENTS program_options)

find_package (GDAL REQUIRED)

# Jonathan Shewchuk's Triangle library.
find_package (Triangle REQUIRED)

include_directories (${Boost_INCLUDE_DIRS} ${GDAL_INCLUDE_DIR} ${Triangle_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})

# Configure a header file to pass cmake settings to the source code.
configure_file ("config.hpp.in" "config.hpp")

add_executable (meshpoly meshpoly.cpp)

target_link_libraries (meshpoly ${Boost_LIBRARIES} ${GDAL_LIBRARY} ${Triangle_LIBRARIES})

install (TARGETS meshpoly RUNTIME DESTINATION bin)
