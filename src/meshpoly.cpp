/*
	Copyright Timothy H. Keitt <tkeitt@gmail.com>, 2012

	This code is released under the same X11/MIT license as GDAL/OGR.
	See http://trac.osgeo.org/gdal/wiki/FAQGeneral#WhatlicensedoesGDALOGRuse

	The Triangle Library by Jonathan Shewchuk can be used freely, but requires
        a license if you sell the code to someone else.
	See http://www.cs.cmu.edu/~quake/triangle.html
*/

#include <iostream>
#include <vector>
#include <string>
#include <gdal/ogrsf_frmts.h>
#include <boost/static_assert.hpp>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>

#include "config.hpp"
#include "triangle_utils.hpp"

namespace po = boost::program_options;

BOOST_STATIC_ASSERT(sizeof(REAL) == sizeof(double));

int
main(int argc, char* argv[])
{

	po::variables_map opts;
	std::vector<char> triopts;
	std::string edges_layer_name("edges"),
                    points_layer_name("nodes"),
                    vpoints_layer_name("voronoi_nodes"),
		    vedges_layer_name("voronoi_edges"),
		    triangles_layer_name("triangles");

	try
	{
		po::options_description opts_shown("Command line options");
		opts_shown.add_options()
			("help", "print program options")
                        ("version", "print program version")
			("quality", "impose quality constraints on min and max angles")
			("minangle", po::value<double>(), "minimum angle constraint (implies quality)")
			("maxarea", po::value<double>(), "maximum triangle area")
			("voronoi", "also output Voronoi edges and nodes")
			("triangles", "also output mesh triangles")
			("base_1", "output element indices starting at 1 instead of 0")
			("conforming", "force triangles to be truly Delaunay")
			("convex_hull", "mesh within convex hull of input")
			("check", "perform consistency check on final triangulation")
			("output_prefix", po::value<std::string>(), "prefix added to output layer names")
//			("layer_options", po::value<std::string>(), "GDAL layer creation options")
			("output_driver", po::value<std::string>(), "output file type (default same as input)")
			("verbose", "print runtime information");

		po::options_description opts_not_shown("Non-printing options");
		opts_not_shown.add_options()
			("input", po::value<std::string>(), "input file")
			("output", po::value<std::string>(), "output file");

		po::positional_options_description pos_opts;
		pos_opts.add("input", 1).add("output", 2);	

		po::options_description opts_desc("All options");
		opts_desc.add(opts_shown).add(opts_not_shown);

		po::store(po::command_line_parser(argc, argv).
	                  options(opts_desc).positional(pos_opts).run(), opts);
		po::notify(opts);

                if ( opts.count("version") )
                {
                  std::cout << meshpoly::version << std::endl;
                  return 0;
                }

		if ( opts.count("help") || (! opts.count("input")) || (! opts.count("output")))
		{
			std::cout << std::endl << "Usage: meshpoly [options] input output"
	                          << std::endl << std::endl << opts_shown << std::endl;
			return 1;
		}

		triopts.push_back('p');
		triopts.push_back('e');
		triopts.push_back('z');
		triopts.push_back(opts.count("verbose") ? 'V' : 'Q');

		if ( opts.count("triangles") ) triopts.push_back('n');
		if ( opts.count("voronoi") ) triopts.push_back('v');
		if ( opts.count("conforming") ) triopts.push_back('D');
		if ( opts.count("check") ) triopts.push_back('C');
		if ( opts.count("convex_hull") ) triopts.push_back('c');

		if ( opts.count("quality") || opts.count("minangle") )
		{
			triopts.push_back('q');
			if ( opts.count("minangle") )
			{
				std::string minangle = boost::lexical_cast<std::string>(opts["minangle"].as<double>());
				std::copy(minangle.begin(), minangle.end(), std::back_inserter(triopts));
			}
		}

		if ( opts.count("maxarea") )
		{
			triopts.push_back('a');
			std::string maxarea = boost::lexical_cast<std::string>(opts["maxarea"].as<double>());
			std::copy(maxarea.begin(), maxarea.end(), std::back_inserter(triopts));
		}

		triopts.push_back('\0');

		// std::copy(triopts.begin(), triopts.end(), std::ostream_iterator<char>(std::cout));

		if ( opts.count("layer_prefix") )
		{
			std::string layer_prefix = opts["layer_prefix"].as<std::string>();
			edges_layer_name = layer_prefix + "_" + edges_layer_name;
			points_layer_name = layer_prefix + "_" + points_layer_name;
			vedges_layer_name = layer_prefix + "_" + vedges_layer_name;
			vpoints_layer_name = layer_prefix + "_" + vpoints_layer_name;
			triangles_layer_name = layer_prefix + "_" + triangles_layer_name;
		}

	}
	catch(std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	        return 1;
	}   

	OGRRegisterAll();

	std::string ifname = opts["input"].as<std::string>();
	OGRDataSource* dsrc = OGRSFDriverRegistrar::Open(ifname.c_str(), FALSE);
	if ( dsrc == NULL )
	{
		std::cerr << "Failed to open data source " << ifname << std::endl;
		return 1;
	}

	// TODO: allow user to choose layer by name or number
	OGRLayer* dlayr = dsrc->GetLayer(0);
	if ( dlayr == NULL )
	{
		std::cerr << "Unable to retreive data layer" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}

	dlayr->ResetReading();
	OGRFeature* ftr = dlayr->GetNextFeature();
	if ( ftr == NULL )
	{
		std::cerr << "Unable to retreive feature" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}
		
	OGRGeometry* geom = ftr->GetGeometryRef();
	geom->closeRings();
	geom->flattenTo2D();	
	if ( geom->getGeometryType() != wkbPolygon )
	{
		std::cerr << "Geometry must be of OGR type wkbPolygon" << std::endl;   // What to do about multipolygons???
		OGRFeature::DestroyFeature(ftr);
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}		

	// TODO: handle holes
	OGRLinearRing* lrng = static_cast<OGRPolygon*>(geom)->getExteriorRing();
	if ( lrng == NULL )
	{
		std::cerr << "Polygon feature is empty" << std::endl;
		OGRFeature::DestroyFeature(ftr);
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}		

	int numpoints = lrng->getNumPoints();
	if ( numpoints < 4 )
	{
		std::cerr << "Polygon feature is empty" << std::endl;
		OGRFeature::DestroyFeature(ftr);
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}		
	
	OGRRawPoint* points = static_cast<OGRRawPoint*>(CPLMalloc(sizeof(OGRRawPoint) * numpoints));
	lrng->getPoints(points);	// I'm not sure if the first and last points are always identical

	OGRFeature::DestroyFeature(ftr);

	triangulateio tri_in, tri_out, tri_out_v;
	initTriIO(&tri_in);
	initTriIO(&tri_out);
	initTriIO(&tri_out_v);

	tri_in.pointlist = (REAL*) points;
	tri_in.numberofpoints = numpoints - 1;		// FIX ME! How to deal with duplicate points???
	tri_in.numberofsegments = tri_in.numberofpoints;

	tri_in.segmentlist = static_cast<int*>(CPLMalloc(sizeof(int) * 2 * tri_in.numberofsegments));
	if ( tri_in.segmentlist == NULL )
	{
		std::cerr << "Memory allocation error" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}
	for ( int i = 0; i < tri_in.numberofsegments - 1; ++i )
	{
		tri_in.segmentlist[2 * i] = i;
		tri_in.segmentlist[2 * i + 1] = i + 1;
	}
	tri_in.segmentlist[2 * tri_in.numberofsegments - 2] = tri_in.numberofpoints - 1;
	tri_in.segmentlist[2 * tri_in.numberofsegments - 1] = 0;

	OGRSFDriver* drv;

	if ( opts.count("output_driver") )
	{
		std::string output_driver_name = opts["output_driver"].as<std::string>();
		drv = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(output_driver_name.c_str());
	}
	else
	{
		drv = dsrc->GetDriver();
	}
	if ( drv == NULL )
	{
		std::cerr << "Unable to load output driver" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}

	std::string ofnam = opts["output"].as<std::string>();
	OGRDataSource* dres = drv->CreateDataSource(ofnam.c_str(), NULL);
	if ( dres == NULL )
	{
		std::cerr << "Unable to create output data source" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		return 1;
	}

	triangulate(&triopts[0], &tri_in, &tri_out, &tri_out_v);

	// TODO: Allow layer creation options
	OGRLayer* edge_layer = dres->CreateLayer(edges_layer_name.c_str(), dlayr->GetSpatialRef(), wkbLineString, NULL);
	if ( edge_layer == NULL )
	{
		std::cerr << "Output layer creation failed" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		OGRDataSource::DestroyDataSource(dres);
		return 1;
	}

	// convert to throw()???
	if ( write_edges_to_layer(&tri_out, edge_layer, opts.count("base_1") ? 1 : 0) )
	{
		OGRDataSource::DestroyDataSource(dsrc);
		OGRDataSource::DestroyDataSource(dres);
		return 1;
	}

	OGRLayer* point_layer = dres->CreateLayer(points_layer_name.c_str(), dlayr->GetSpatialRef(), wkbPoint, NULL);
	if ( point_layer == NULL )
	{
		std::cerr << "Output layer creation failed" << std::endl;
		OGRDataSource::DestroyDataSource(dsrc);
		OGRDataSource::DestroyDataSource(dres);
		return 1;
	}

	if ( write_points_to_layer(&tri_out, point_layer) )
	{
		OGRDataSource::DestroyDataSource(dsrc);
		OGRDataSource::DestroyDataSource(dres);
		return 1;
	}

	if ( opts.count("voronoi") )
	{
	
		OGRLayer* voronoi_edges_layer = dres->CreateLayer(vedges_layer_name.c_str(), dlayr->GetSpatialRef(), wkbLineString, NULL);
		if ( voronoi_edges_layer == NULL )
		{
			std::cerr << "Output layer creation failed" << std::endl;
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}

		if ( write_voronoi_edges_to_layer(&tri_out_v, voronoi_edges_layer, opts.count("base_1") ? 1 : 0) )
		{
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}
	
		OGRLayer* voronoi_points_layer = dres->CreateLayer(vpoints_layer_name.c_str(), dlayr->GetSpatialRef(), wkbPoint, NULL);
		if ( voronoi_points_layer == NULL )
		{
			std::cerr << "Output layer creation failed" << std::endl;
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}

		if ( write_voronoi_points_to_layer(&tri_out_v, voronoi_points_layer) )
		{
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}

	} // Voronoi

	if ( opts.count("triangles") )
	{
	
		OGRLayer* triangles_layer = dres->CreateLayer(triangles_layer_name.c_str(), dlayr->GetSpatialRef(), wkbPolygon, NULL);
		if ( triangles_layer == NULL )
		{
			std::cerr << "Output layer creation failed" << std::endl;
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}

		if ( write_triangles_to_layer(&tri_out, triangles_layer, opts.count("base_1") ? 1 : 0) )
		{
			OGRDataSource::DestroyDataSource(dsrc);
			OGRDataSource::DestroyDataSource(dres);
			return 1;
		}
	
	} // triangles

	freeTriIO(&tri_in);
	freeTriIO(&tri_out);
	freeTriIO(&tri_out_v);

	OGRDataSource::DestroyDataSource(dsrc);
	OGRDataSource::DestroyDataSource(dres);

	return 0;
}
