# Synopsis

Meshpoly builds triangular meshes within a polygon. It is a simple OGR wrapper
around Jonathan Shewchuk's Triangle library. As such, you can use any OGR data source
and output. (Layer options are not yet implemented, so e.g. a postgresql data source
cannot be specified.) As an example:

	> ogrinfo lscape1
	INFO: Open of `lscape1'
	      using driver `ESRI Shapefile' successful.
	1: regions (Polygon)
	2: boundary (Polygon)
	
	> meshpoly --minangle 30 \
	           --maxarea 0.01 \
	           --voronoi \
	           --triangles \
	           --base_1 \
	           --check \
	           --verbose \
	           --output_driver GML \
	           lscape1 testout
	
	> ogrinfo testout
	Had to open data source read-only.
	INFO: Open of `testout'
	      using driver `GML' successful.
	1: edges
	2: nodes
	3: voronoi_edges
	4: voronoi_nodes
	5: triangles

A number of useful features are not yet implemented. See the source for comments.
We would be happy to have patches and feature requests.

# Dependencies

To build this software you will need at least:

1.  [Boost](http://www.boost.org/)
2.  [GDAL](http://www.gdal.org/)
3.  Jonathan Shewchuk's [Triangle](http://www.cs.cmu.edu/~quake/triangle.html) library.

# Installation

    $ cd build
    $ cmake ..
    $ make

# Usage

    Usage: meshpoly [options] input output
    
    Command line options:
      --help                print program options
      --quality             impose quality constraints on min and max angles
      --minangle arg        minimum angle constraint (implies quality)
      --maxarea arg         maximum triangle area
      --voronoi             also output Voronoi edges and nodes
      --triangles           also output mesh triangles
      --base_1              output element indices starting at 1 instead of 0
      --conforming          force triangles to be truly Delaunay
      --convex_hull         mesh within convex hull of input
      --check               perform consistency check on final triangulation
      --output_prefix arg   prefix added to output layer names
      --output_driver arg   output file type (default same as input)
      --verbose             print runtime information

# License

Meshpoly is copyrighted by Timothy H. Keitt (http://www.keittlab.org/).
Meshpoly is released under the X11/MIT license used by GDAL/OGR.
See http://trac.osgeo.org/gdal/wiki/FAQGeneral#WhatlicensedoesGDALOGRuse
The Triangle Library by Johnathan Shewchuk can be used freely, but requires
a license if want to sell the code to someone else.


