#
# Find Jonathan Shewchuk's Triangle include directory and library, and set these variables:
#
# Triangle_FOUND
# Triangle_INCLUDE_DIRS
# Triangle_LIBRARIES

include (LibFindMacros)

# Optional user supplied search path.
set (Triangle_PREFIX_PATH "" CACHE PATH "Directory to search for Triangle header and library files")

# Find include directory.
find_path (Triangle_INCLUDE_DIR triangle.h ${Triangle_PREFIX_PATH})

# Find library.
find_library (Triangle_LIBRARY triangle-1.6 ${Triangle_PREFIX_PATH})

set (Triangle_PROCESS_INCLUDES Triangle_INCLUDE_DIR)
set (Triangle_PROCESS_LIBS Triangle_LIBRARY)

libfind_process (Triangle)
